import torch
# load custom class for model:
from CNN_Model_GTZAN_31_05_21_21p49 import CNN
from torchsummary import summary
from torchvision import transforms, datasets
from torch.utils.data import DataLoader, random_split

# Set device:
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(torch.cuda.get_device_name(0))

# Hyperparameters:
batch_size = 64
image_size = 64
img_channels = 3
learning_rate = 1e-4
num_classes = 10
num_epochs = 100

trafo = transforms.Compose( [transforms.Resize(size = (64, 64)), transforms.ToTensor()] )

gtzan_dataset = datasets.ImageFolder(
                    root = "Mel-Specs-2s",
                    transform = trafo,
                    target_transform = None,
                    is_valid_file = None
)
print(len(gtzan_dataset)) # output: 14465
train_subset, val_subset, test_subset = random_split(dataset = gtzan_dataset, lengths = [10126, 2170, 2169])

train_loader = DataLoader(dataset = train_subset, shuffle = True, batch_size = batch_size)
val_loader = DataLoader(dataset = val_subset, shuffle = True, batch_size = batch_size)
test_loader = DataLoader(dataset = test_subset, shuffle = True, batch_size = batch_size)

print("We have in total {} number of samples, here Mel spectrograms, to train our CNN with.".format(len(train_subset)))
print("We have in total {} number of samples, here Mel spectrograms, to validate our CNN with.".format(len(val_subset)))
print("We have in total {} number of samples, here Mel spectrograms, to test our CNN with.".format(len(test_subset)))

# print stuff:
train_image_x, train_target_x = train_subset[0]
print(train_subset[0])
print(train_image_x.size())

# Print model summary:
model = CNN(img_channels = img_channels, num_classes = num_classes, image_size = image_size).to(device)
x = torch.randn((batch_size, img_channels, image_size, image_size)) # shape: (batch_size, img_channels, image_size, image_size)
print("\n\n", summary(model, (3, 64, 64)))
