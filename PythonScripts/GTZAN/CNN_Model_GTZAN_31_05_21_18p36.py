import torch
import torch.nn as nn # All neural network modules, nn.Linear, nn.Conv2D, nn.InstanceNorm2d, loss functions etc.
import torch.nn.functional as F # ReLU activation function, etc.

# Create CNN:
class CNN(nn.Module):
    """This CNN is trained on the Mel spectrograms of the GTZAN dataset."""
    def __init__(self, img_channels = 3, num_classes = 10):
        """
        Parameters:
            img_channels:   Channels of the Mel spectrograms
            num_classes:    Number of genres in GTZAN dataset

        """
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(
            in_channels = img_channels,
            out_channels = 8,
            kernel_size = (3, 3),
            stride = (1, 1),
            padding = (1, 1),
        )
        self.pool = nn.MaxPool2d(kernel_size = (2, 2), stride = (2, 2))
        self.conv2 = nn.Conv2d(
            in_channels = 8,
            out_channels = 16,
            kernel_size = (3, 3),
            stride = (1, 1),
            padding = (1, 1),
        )
        self.conv3 = nn.Conv2d(
            in_channels = 16,
            out_channels = 32,
            kernel_size = (3, 3),
            stride = (1, 1),
            padding = (1, 1),
        )
        # self.fc1 = nn.Linear(64 * 8 * 8, num_classes)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.pool(x)
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        x = F.relu(self.conv3(x))
        x = self.pool(x)
        x = x.reshape(x.shape[0], -1)
        # x = self.fc1(x)
        return x
