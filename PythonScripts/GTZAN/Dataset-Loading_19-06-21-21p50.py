import os
import sys
import time
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
from datetime import datetime
import matplotlib.pyplot as plt
from torchsummary import summary
import matplotlib.ticker as ticker
from prettytable import PrettyTable
from torchvision import transforms, datasets
# load custom class for model:
from CNN_Model_GTZAN_20_06_21_13p09 import CNN
from torch.utils.data import DataLoader, random_split

# Set device:
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(torch.cuda.get_device_name(0))

# Hyperparameters:
batch_size = 64
image_size = 64
img_channels = 3
learning_rate = 1e-4
num_classes = 10
num_epochs = 100

trafo = transforms.Compose( [transforms.ToTensor()] )

gtzan_dataset = datasets.ImageFolder(
                    root = "Datasets/CNN/Mel-Specs-GTZAN-2s-19-06-2021",
                    transform = trafo,
                    target_transform = None,
                    is_valid_file = None
)
length = len(gtzan_dataset)
print(length) # output: 15000
train_split, val_split, test_split = (0.8, 0.1, 0.1)
train_subset, val_subset, test_subset = random_split(dataset = gtzan_dataset, lengths = [int(train_split * length), int(val_split * length), int(test_split * length)])

train_loader = DataLoader(dataset = train_subset, shuffle = True, batch_size = batch_size)
val_loader = DataLoader(dataset = val_subset, shuffle = True, batch_size = batch_size)
test_loader = DataLoader(dataset = test_subset, shuffle = True, batch_size = batch_size)

print("We have in total {} number of samples, here Mel spectrograms, to train our CNN with.".format(len(train_subset)))
print("We have in total {} number of samples, here Mel spectrograms, to validate our CNN with.".format(len(val_subset)))
print("We have in total {} number of samples, here Mel spectrograms, to test our CNN with.".format(len(test_subset)))

# print stuff:
train_image_x, train_target_x = train_subset[0]
print(train_subset[0])
print(train_image_x.size())

# Print model summary:
model = CNN(img_channels = img_channels, num_classes = num_classes, image_size = image_size).to(device)
x = torch.randn((batch_size, img_channels, image_size, image_size)) # shape: (batch_size, img_channels, image_size, image_size)
print("\n\n", summary(model, (img_channels, image_size, image_size)))

# Initialize CNN:
model = CNN(img_channels = img_channels, num_classes = num_classes, image_size = image_size).to(device)

# Loss and optimizer:
lossFunction = nn.CrossEntropyLoss() # https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html
optim = optim.Adam(params = model.parameters(), lr = learning_rate, betas = (0.9, 0.999), eps = 1e-08)

# Create checkpoint function:
def save_checkpoint(state, filename = "my_checkpoint.pth.tar"):
    """Creates Model Checkpoint to Save And Load a Model.
    .pth.tar is commonly used for this."""
    torch.save(state, filename)
    print("=> Saving checkpoint")

# Set network to train mode:
model.train()

# Train CNN:
start_time = time.time()
# arrays for plotting:
train_losses = np.array([])
val_losses = np.array([])
train_accuracies = np.array([])
val_accuracies = np.array([])

for epoch in range(num_epochs):
    # store losses to print for each epoch
    trainingLoss_perEpoch = np.array([]) # training loss per epoch
    valLoss_perEpoch = np.array([]) # val loss per epoch
    # initialize epoch time:
    t0 = datetime.now()

    # variables for calculating the accuracy:
    num_correct = 0
    num_samples = 0
    val_num_correct = 0 # for the validation accuracy
    val_num_samples = 0 # for the validation accuracy

    for batch_idx, (images, labels) in enumerate(train_loader):
        # Reshape data & get to cuda if possible
        images = images.to(device) # shape: (batch_size, 1, 28, 28)
        labels = labels.to(device)
        batch_size = images.shape[0]
        # Train model (forward pass):
        output = model(images)
        loss = lossFunction(output, labels)

        # Calculate accuracy:
        model.eval()
        output_maxima, max_indices = torch.max(output, dim = 1, keepdim = False)
        # from our model, we get predictions of the shape [batch_size, C], where C is the num of classes and in the case of MNIST, C = 10
        num_correct += torch.sum(input = (max_indices == labels))
        num_samples += batch_size
        assert batch_size == max_indices.size(0), "The first index of output of the forward pass and the batch size do not agree!!"
        # max_indices.size(0) is already an int

        model.train()
        # backprop:
        optim.zero_grad()
        loss.backward()
        # Gradient descent step:
        optim.step()

        # Append training loss to array for averaging:
        trainingLoss_perEpoch = np.append(trainingLoss_perEpoch, loss.detach().cpu())

        if batch_idx == 0:
            # Code for printouts concerning progress of training:
            print("Train Epoch: %d [ %05d / %d (%f %%)]\tTraining Loss: %f"
                    %(epoch, batch_idx*len(images), len(train_loader.dataset), 100.*batch_idx/len(train_loader), np.round_(loss.detach().cpu(), decimals=4))
            )
        if batch_idx % 10 == 0 and batch_idx != 0:
            # Code for printouts concerning progress of training:
            print("Train Epoch: %d [ %05d / %d (%f %%)]\tTraining Loss: %f \tElapsed Time: %f s"
                   %(epoch, batch_idx * len(images), len(train_loader.dataset),
                   100.*batch_idx/len(train_loader), np.round_(loss.detach().cpu(), decimals=4), np.round_((datetime.now()-t0).total_seconds(), decimals=2))
            )

    # validation stuff:
    model.eval()
    for val_batch_idx, (val_images, val_labels) in enumerate(val_loader):
        # print(val_images.shape)
        val_images = val_images.to(device)
        val_labels = val_labels.to(device)
        batch_size = val_images.shape[0]
        # Forward pass on the val dataset (no backprop!):
        val_output = model(val_images)
        val_loss = lossFunction(val_output, val_labels)

        # Calculate accuracy:
        val_output_maxima, val_max_indices = torch.max(val_output, dim = 1, keepdim = False)
        # from our model, we get predictions of the shape [batch_size, C], where C is the num of classes and in the case of MNIST, C = 10
        val_num_correct += torch.sum(input = (val_max_indices == val_labels))
        val_num_samples += batch_size
        assert batch_size == val_max_indices.size(0), "The first index of output of the forward pass and the batch size do not agree!!"
        # val_max_indices.size(0) is already an int

        # Append val loss to array for averaging:
        valLoss_perEpoch = np.append(valLoss_perEpoch, loss.detach().cpu())

        if val_batch_idx == 0:
            # Code for printouts concerning progress of training:
            print("Valid Epoch: %d [ %05d / %d (%f %%)]\tValidation Loss: %f"
                    %(epoch, val_batch_idx*len(val_images), len(val_loader.dataset), 100.*val_batch_idx/len(val_loader), np.round_(val_loss.detach().cpu(), decimals=4))
            )
        if val_batch_idx % 20 == 0 and val_batch_idx != 0:
            # Code for printouts concerning progress of training:
            print("Valid Epoch: %d [ %05d / %d (%f %%)]\tValidation Loss: %f \tElapsed Time: %f s"
                   %(epoch, val_batch_idx * len(val_images), len(val_loader.dataset),
                   100.*val_batch_idx/len(val_loader), np.round_(val_loss.detach().cpu(), decimals=4), np.round_((datetime.now()-t0).total_seconds(), decimals=2))
            )

    # calculate accuracies for each epoch:
    train_acc = int(num_correct) / num_samples
    train_accuracies = np.append(train_accuracies, train_acc)
    # for validation:
    val_acc = int(val_num_correct) / val_num_samples
    val_accuracies = np.append(val_accuracies, val_acc)
    # append training losses to corresponding arrays for plotting:
    train_losses = np.append(train_losses, np.average(trainingLoss_perEpoch))
    val_losses = np.append(val_losses, np.average(valLoss_perEpoch))

    print("Epoch {:02}: {:.2f} sec ...".format(epoch, (datetime.now() - t0).total_seconds(), end = "\n\n"))
    print("Averaged Training loss: ", round(np.average(trainingLoss_perEpoch), 4), "\tTraining accuracy: {:.2f}".format(train_acc*100), "%",  end = "\n")
    print("Averaged Validation loss: ", round(np.average(valLoss_perEpoch), 4), "\tValidation accuracy: {:.2f}".format(val_acc*100), "%", end = "\n\n\n")

    model.train()
print("The whole training of {} epoch(s) took {} seconds".format(num_epochs, round(time.time() - start_time , 2)))
# Create checkpoint variables:
checkpoint = {'state_dicts' : [model.state_dict()], 'optimizers': [model.state_dict()]}
# Save one checkpoint at the end of training:
saving_path = "CNN/Trainings/GTZAN/CNN-v8-19-06-21/"
save_checkpoint(state=checkpoint,
                filename = os.path.join(
                            saving_path, "CNN-lr-" + str(learning_rate) + "-batch-size-" + str(64) +
                            "-" + datetime.now().strftime("%d-%m-%Y-%H:%M")+".pth.tar"
                            )
)

# check accuracy:
def check_accuracy(loader, model):
    model.eval()
    num_correct = 0
    num_samples = 0

    with torch.no_grad(): # don't waste computation by calculating gradients when we don't need them
        for (images, labels) in loader:
            images = images.to(device = device)
            images = torch.squeeze(input = images, dim = 1) # shape: (batch_size, 28, 28), otherwise RNN throws error
            labels = labels.to(device = device)

            forward_pass = model(images) # shape: (batch_size, 10)
            _, predictions = forward_pass.max(dim = 1) # from our model, we get the shape (batch_size, 10) returned
            num_correct += (predictions == labels).sum()
            num_samples += predictions.size(0)

        print(f"Got {num_correct}/{num_samples} with accuracy {float(num_correct)/float(num_samples) * 100:.2f} /")

print("Checking accuracy on training data:")
check_accuracy(train_loader, model)
print("Checking accuracy on test data:")
check_accuracy(test_loader, model)

# Plot:
epochs = np.arange(start = 0, stop = num_epochs, step = 1)

# plot training loss:
fig, ax = plt.subplots()
loc = ticker.MultipleLocator(base = 5.0)
ax.xaxis.set_major_locator(loc)

plt.plot(epochs, train_losses, label = "Training")
plt.plot(epochs, val_losses, label = "Validation")
plt.xlabel("Epoch")
plt.ylabel("Loss (Categorical Crossentropy)")
plt.legend()
plt.savefig(os.path.join(saving_path + "loss-lr-" + str(learning_rate) + "-batch-size-" + str(64) + "-" + datetime.now().strftime('%d-%m-%Y-%H:%M') + ".pdf"))
plt.close()

# plot training accuracy:
fig, ax = plt.subplots()
loc = ticker.MultipleLocator(base = 5.0)
ax.xaxis.set_major_locator(loc)

plt.plot(epochs, train_accuracies, label = "Training")
plt.plot(epochs, val_accuracies, label = "Validation")
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
plt.legend()
plt.savefig(os.path.join(saving_path + "accuracy-lr-" + str(learning_rate) + "-batch-size-" + str(64) + "-" + datetime.now().strftime('%d-%m-%Y-%H:%M') + ".pdf"))
plt.close()

# Save the arrays in npy format as well:
np.savez(os.path.join(saving_path, "CNN-lr-" + str(learning_rate) + "-batch-size-" + str(64)),
         train_losses, val_losses, train_accuracies, val_accuracies
)
# Don't write <file=saving_path+"CNN-lr-"+...>, because then I get the following error: <SyntaxError: positional argument follows keyword argument>

### CREATE CONFUSION MATRIX:
confusion_matrix = torch.zeros(num_classes, num_classes)
counter = 0
with torch.no_grad():
    model.eval()
    for i, (inputs, classes) in enumerate(test_loader):
        inputs = inputs.to(device)
        inputs = torch.squeeze(input = inputs, dim = 1) # shape: (batch_size, 28, 28), otherwise RNN throws error
        classes = classes.to(device)
        outputs = model(inputs)
        _, preds = torch.max(outputs, 1)
        for t, p in zip(classes.view(-1), preds.view(-1)):
            confusion_matrix[t, p] += 1
            counter += 1

# Because of the random split in the datasets, the classes are imbalanced. Thus, we should do a normalization across each label in the confusion matrix:
for i in range(num_classes):
    total_sums = 0
    for element in confusion_matrix[i]:
        total_sums += element
    confusion_matrix[i] /= total_sums

print("Confusion matrix:", confusion_matrix)

# Convert PyTorch tensor to numpy array:
confusion_matrix = confusion_matrix.detach().cpu().numpy()
# Labes neural network should learn:
name_genres = np.array(["blues", "classical", "country", "disco", "hiphop", "jazz", "metal", "pop", "reggae", "rock"])

(width, height) = plt.rcParams['figure.figsize'] # Default matplotlib values for figure size
(width, height) = (int(1.6*width), int(1.6*height))

fig = plt.figure(figsize = (width, height))
plt.imshow(confusion_matrix, cmap = "jet")
plt.colorbar(ticks=np.arange(0, 1.0, 0.15))
tick_marks = np.arange(num_classes)
plt.xticks(tick_marks, name_genres, rotation = 45)
plt.yticks(tick_marks, name_genres)
plt.xlabel("Predicted Labels", weight = "bold")
plt.ylabel("True Labels", weight = "bold")
plt.savefig(os.path.join(saving_path, "confusion_matrix.pdf"), bbox_inches = "tight")

np.savez(os.path.join(saving_path, "confusion_matrix"), A = confusion_matrix)

# End of file:
print("Finished")

### REFERENCES:
# https://stackoverflow.com/questions/64092369/validation-dataset-in-pytorch-using-dataloaders
# https://www.geeksforgeeks.org/training-neural-networks-with-validation-using-pytorch/
# https://stackoverflow.com/questions/49201236/check-the-total-number-of-parameters-in-a-pytorch-model
# https://yewtu.be/watch?v=wnK3uWv_WkU (for checking accuracy)
# https://stackoverflow.com/questions/36162414/how-to-add-bold-annotated-text-in-matplotlib (bold labels in matplotlib plot)

### Main changes in this file: Nicer confusion matrix
