import os
import sys
import librosa
import numpy as np
import pandas as pd
from PIL import Image
import librosa.display
from datetime import datetime
import matplotlib.pyplot as plt

# Parameters when loading the .wav audio files:
durationLength = 5 # seconds, previously: 2 seconds
totalDuration = 30 # seconds
type_data = "small" # decide whether to use <fma_small> or <fma_medium> dataset

# List of our genres
if type_data == "small":
    genres = ["Hip-Hop", "Pop", "Folk", "Experimental", "Rock", "International", "Electronic", "Instrumental"]
else:
    genres = ["Hip-Hop", "Rock", "Folk", "Jazz", "Electronic", "Experimental", "Soul-RnB", "Pop", "Blues", "Spoken", "Country",
              "Classical", "Old-Time / Historic", "Instrumental", "International", "Easy Listening"
    ]

# counter:
counter = 0

# Read in csv-file:
dframe = pd.read_csv("Datasets/fma_metadata/tracks.csv")
# Convert column with track-ids from type string to numerical type (float or int):
dframe["Unnamed: 0"] = pd.to_numeric(dframe["Unnamed: 0"], errors = "coerce")

# Create necessary directories
directory_name = "Datasets/CNN/MelSpecs-fma-" + type_data + "-" + str(durationLength) + "s-" + datetime.now().strftime("%d-%m-%Y")
# Make sure that the directories that we want to create do not already exist
print(os.path.exists(directory_name))
assert os.path.exists(directory_name) == False, "The directory you want to create already exists!"
os.mkdir(directory_name)

# Create subdirectories where the images shall be stored:
for genre in genres:
    subdirectory_name = os.path.join(directory_name, genre)
    assert os.path.exists(subdirectory_name) == False, "At least one of the subdirectories you want to create already exists!"
    os.mkdir(subdirectory_name)

# Directory where the .mp3 files are stored:
source_files = "Datasets/fma_" + type_data + "/"

for folder in os.listdir(source_files):
    subfolder_name = os.fsdecode(folder)
    print(subfolder_name)

    for filename in os.listdir(os.path.join(source_files, subfolder_name)):
        # filename = os.fsdecode(file), I made bad experience with this in this specific case.
        filename_original = filename
        filename_stripped = filename[:-4].lstrip("0")
        filename_stripped = int(filename_stripped)
        # Determine label for .mp3 file:
        genre = dframe.loc[dframe["Unnamed: 0"] == filename_stripped]["track.7"].item()

        for i in range(int(totalDuration/durationLength)):
            # Load audio file with librosa:
            song, sr = librosa.load(os.path.join(source_files, subfolder_name, filename_original), sr = 22050, mono = True, offset = durationLength*i, duration = durationLength)
            print(filename_original + " successfully loaded.")

            # Create Mel spectrograms with librosa library:
            songMelspec = librosa.feature.melspectrogram(song)

            # Convert amplitudes to dB
            songMelspec = librosa.amplitude_to_db(songMelspec, ref = np.max)

            # Produce Mel spectrograms:
            fig = plt.figure(frameon = False)
            songDisplay = librosa.display.specshow(songMelspec)
            # For proper saving:
            plt.gca().set_axis_off()
            plt.gca().xaxis.set_major_locator(plt.NullLocator())
            plt.gca().yaxis.set_major_locator(plt.NullLocator())
            # Create saving_path for "automated" saving of the files:
            saving_path = os.path.join(directory_name, genre, str(filename_stripped) + "-" + str(i))
            # Save the Mel spectrograms:
            plt.savefig(saving_path + ".jpg", bbox_inches='tight', pad_inches=0)
            plt.close()

            # Resize Mel spectrograms to (64, 64):
            im = Image.open(saving_path + ".jpg")
            im_out = im.resize((64, 64))
            im_out.save(saving_path + ".jpg")

            counter += 1

print("counter:", counter)
print("--- Finito ---")

## Reference for proper saving:
# https://stackoverflow.com/questions/11837979/removing-white-space-around-a-saved-image-in-matplotlib
