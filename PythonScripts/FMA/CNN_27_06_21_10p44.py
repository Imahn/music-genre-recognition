import torch
import torch.nn as nn # All neural network modules, nn.Linear, nn.Conv2D, nn.InstanceNorm2d, loss functions etc.
import torch.nn.functional as F # ReLU activation function, etc.

# Create CNN:
class CNN(nn.Module):
    """This CNN is trained on the Mel spectrograms of the GTZAN dataset."""
    def __init__(self, img_channels = 3, num_classes = 10, image_size = 64, dropout_rate = 0.2):
        """
        Parameters:
            img_channels (int):   Channels of the Mel spectrograms
            num_classes (int):    Number of genres in GTZAN dataset
            image_size (int):     The image size of the input Mel spectrograms
            dropout_rate (float): Dropout rate (number between [0.0, 1.0)
        """
        super(CNN, self).__init__()
        # Parameters for nn.Conv2d() layer:
        kernel_size = (3, 3)
        stride = (1, 1)
        padding = (1, 1)
        # number of out_channels in first Conv2d layer:
        nout_channels = 8

        self.conv1 = self._block(in_channels=img_channels, out_channels=nout_channels, kernel_size=kernel_size, stride=stride, padding=padding)
        # writing "_block" instead of "self._block" results in an error message
        self.conv2 = self._block(in_channels=nout_channels, out_channels=2*nout_channels, kernel_size=kernel_size, stride=stride, padding=padding)
        self.pool = nn.MaxPool2d(kernel_size=(2, 2), stride=(2, 2))

        self.dropout = nn.Dropout(p = dropout_rate, inplace = False)

        self.conv3 = self._block(in_channels=2*nout_channels, out_channels=4*nout_channels, kernel_size=kernel_size, stride=stride, padding=padding)
        self.conv4 = self._block(in_channels=4*nout_channels, out_channels=8*nout_channels, kernel_size=kernel_size, stride=stride, padding=padding)

        self.conv5 = self._block(in_channels=8*nout_channels, out_channels=16*nout_channels, kernel_size=kernel_size, stride=stride, padding=padding)

        # We apply same-convolutions, so we can (analytically) calculate the shape for nn.Linear, assuming that the input images are of shape (64, 64)
        self.fc1 = nn.Linear(16*nout_channels * int(image_size/8)**2, 256)
        self.fc2 = nn.Linear(256, num_classes)
        # writing "(image_size/4)**2" without the int() operator results in an error, because image/size*4 is of type float.

        self.batchnorm1 = nn.BatchNorm2d(num_features = 2*nout_channels, eps = 1e-5, momentum = 0.1, affine = True)
        self.batchnorm2 = nn.BatchNorm2d(num_features = 8*nout_channels, eps = 1e-5, momentum = 0.1, affine = True)
        self.batchnorm3 = nn.BatchNorm2d(num_features = 16*nout_channels, eps = 1e-5, momentum = 0.1, affine = True)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        x = self.batchnorm1(x)
        x = self.dropout(x)

        x = F.relu(self.conv3(x))
        x = F.relu(self.conv4(x))
        x = self.pool(x)
        x = self.batchnorm2(x)
        x = self.dropout(x)

        x = F.relu(self.conv5(x))
        x = self.pool(x)
        x = self.batchnorm3(x)
        x = self.dropout(x)

        x = x.reshape(x.shape[0], -1)
        x = F.relu(self.fc1(x))
        x = self.dropout(x)
        x = self.fc2(x)
        return x

    def _block(self, in_channels, out_channels, kernel_size, stride, padding):
        """Use this block to simplify repetitive blocks for Conv2d layers.

        Parameters:
            in_channels (int):              Number of receiving in_channels.
            out_channels (int):             Number of outgoing channels, also called number of filters.
            kernel_size (int or tuple):     Parameter for nn.Conv2d().
            stride (int or tuple):          Parameter for nn.Conv2d().
            padding (int or tuple):         Parameter for nn.Conv2d().

        Returns an nn.Conv2d layer from PyTorch.
        """
        return nn.Conv2d(
            in_channels = in_channels,
            out_channels = out_channels,
            kernel_size = kernel_size,
            stride = stride,
            padding = padding,
        )
