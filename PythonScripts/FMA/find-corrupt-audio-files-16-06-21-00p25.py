import os
import mutagen
import numpy as np
import pandas as pd
from mutagen.mp3 import MP3

# preliminary work:
durationLength = 2 # seconds
totalDuration = 30 # seconds
type_data = "small" # decide whether to use <fma_small> or <fma_medium> dataset
corrupt_files = np.array([])

# Directory where the .mp3 files are stored:
source_files = "Datasets/fma_" + type_data + "/"

for folder in os.listdir(source_files):
    subfolder_name = os.fsdecode(folder)

    for file in os.listdir(os.path.join(source_files, subfolder_name)):
        file_name = os.fsdecode(file)
        audio_path = os.path.join(source_files, subfolder_name, file_name)
        audio = MP3(audio_path)
        print(file_name + "successfully loaded.")

        if audio.info.length < (totalDuration - durationLength): # seconds
            corrupt_files = np.append(corrupt_files, file_name)
            print(file_name, audio.info.length)

print("--- Finito ---")
