import os
import sys
import librosa
import numpy as np
import pandas as pd
from PIL import Image
import librosa.display
from datetime import datetime
import matplotlib.pyplot as plt

# durationLength in seconds:
durationLength = 2

corrupt_files = np.array(["098565.mp3", "098567.mp3", "098569.mp3"])

for file in corrupt_files:
    audio_path = os.path.join("Datasets/fma_small/098", file)
    song, sr = librosa.load(audio_path, sr = 22050, mono = True, offset = 0, duration = durationLength)
    audio_path = os.path.join("Datasets/fma_small/098")
    print(audio_path)
