# Music Genre Recognition

Music genre recognition deals with the classification of music genres given raw audio files as input. This is a repository using CNN's on the GTZAN and the FMA dataset. 

*NOTE: I worked with fellow students on this project in a private git repository, and I have decided myself to publish my work and scripts including the results. This project was done under the supervision of Prof. Christina Brandt (Mathematics faculty, University of Hamburg), and I had been the project group leader.*

## Preprocessing: Mel Spectrograms

Obviously, an audio file cannot be fed directly into a neural network - for this, it must be **preprocessed** first. In this project, I found that using **Mel spectograms** works reasonably well. 

- Mel spectrograms rely on the Fourier transformation, which can mathematically be described as follows: let $`f:\mathbb{R}\rightarrow\mathbb{C}`$ be such that $f\in L^{1}(\mathbb{R})$, then the Fourier transformation of $`f`$ is given by 
```math
\hat{f}(w) := \frac{1}{\sqrt{2\pi}}\int_{\mathbb{R}}f(t)e^{-it\omega}dt, \omega\in \mathbb{R}
```

- By using the Mel instead of frequency scale, one can make use of the fact that the human ear perceives lower and higher frequencies differently. The mel scale is given as follows: 
```math
m := 1127\cdot\ln\left\{ 1 + \frac{\omega}{700\ \text{Hz}} \right\}
```

- A Mel spectrogram is now a 3D representation of an audio signal, where on the x-axis, the time is shown, on the y-axis, the Mel scale, and the z-scale is proportional to the signal. Often times, the z-axis is omitted and a heatmap used instead. 

- Conveniently, there is a Python library called **librosa** (https://librosa.org/) that allows computing and displaying Mel spectrograms out of an audio file. Additionally, a transformation into the dB scale is possible. 

For the FMA dataset (see below), I produced a few Mel spectrograms corresponding to the different genres. Here, I chose 2 seconds of a song for one Mel spectrogram, which is also something I did for training, i.e. every song is divided into 2 second intervals and then for every 2 second interval, a Mel spectrogram is saved and fed into a CNN. 

![mel_spectrogram_electronic](Images/mel_spectrogram_electronic.png)

![mel_spectrogram_experimental](Images/mel_spectrogram_experimental.png)

![mel_spectrogram_international](Images/mel_spectrogram_international.png)

![mel_spectrogram_rock](Images/mel_spectrogram_rock.png)


## Datasets 

### GTZAN Dataset

- This dataset was first used in the paper *Musical Genre Classification of Audio Signals* by G. Tzanetakis and P. Cook
- It is an often dataset used for music genre classification
- There are audio files corresponding to in total 10 genres: blues, classical, country, disco, hip-hop, jazz, metal, pop, reggae, rock
- The provided audio files are only 30 seconds long.
- The audio files were collected from various sources: microphone recordings, personal CDs and radio
- Official metadata missing, i.e. they are not included by default

For the GTZAN dataset, this is the network architecture that I used:

![gtzan_confusion_matrix](Images/gtzan_architecture_final.png)

The used dropout rate is $`20\%`$, and after every convolutional layer, the ReLU activation function is used. After the first fully-connected layer, which is called `Linear` in the network summary, the ReLU activation function follows again.

As you can see, the following block is repeated: 
_ _ _ _ _ _ _ 
Conv2D
ReLU
Conv2D
ReLU
MaxPool2D
BatchNorm2D
Dropout
_ _ _ _ _ _ _ _
Afterwards, another such block is used, with the only difference that there is only one instead of two Conv2D layers to save parameters. 


### FMA Dataset
- Originally proposed in arXiv:1612.01840v3
- I used the small dataset with 7994 audio samples (originally 8000, 6 had to be removed) as proof-of-concept
- 8 genres: Experimental, Folk, Hip-Hop, Instrumental,
International, Pop, Rock

The same architecture as for the GTZAN is used (see above), but the very last block has two Conv2D layers. This improved the performance by more than $`1\%`$ in the final test accuracy. Also, the training slightly differs in that cyclical learning rates were used here, which improved the performance as well. For a motivation of using cyclical learning rates, I can recommend the following paper: arXiv:1506.01186. 


## Visuals
For the GTZAN dataset, this is how the obtained confusion matrix looks like: 

![gtzan_confusion](Images/confusion_matrix_gtzan.png)

As you can see, pop, rock and country are the genres that are classified the worst. Particularly, for pop and rock I suspect that those genres are so popular that artists might misuse these genres for financial reasons and number of albums sold. 

For the FMA dataset, I obtained a final test accuracy of $`74.72\%`$. Here is the confusion matrix: 

![fma_confusion](Images/confusion_matrix_fma.png)

Clearly, pop has a very poor performance with a correct classification in only $`55.65\%`$ of the cases. On the other hand, hip-hip and rock perform very well. 

## Usage
Run 
```python
python -B PythonScripts/GTZAN/Dataset-Loading_21-06-21-20p51.py 
```
Please note that here, argparsing is not used yet (only for the FMA script), so please change the hyperparameters in these lines: https://gitlab.com/Imahn/music-genre-recognition/-/blob/main/PythonScripts/GTZAN/Dataset-Loading_21-06-21-20p51.py#L25-L30

In particular, choose 
```python
num_epochs = 100
```
to obtain the same results as I do. 

For the FMA dataset, run 
```python
python -B PythonScripts/FMA/Dataset-Loading-cosine-lr_14-08-21.py --savingPath "Trainings/FMA/2s/CNN/weight-decay-cosine/trial-1/" --weightDecay 2e-5 --numEpochs 200 --TMax 15
```

## Support
Please open an issue or file a PR if you find bugs or have other ideas! 


## Possible next steps
Applying the ResNets, as I developed them [https://gitlab.com/Imahn/resnet](here), might be interesting to try. This might give quite a performance boost. 
